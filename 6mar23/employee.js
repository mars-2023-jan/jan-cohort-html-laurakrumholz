var employeesArr = [] // create an array to store employee object when click save

function addEmployee(){

    // get values from form
    let empName = document.getElementById("empname").value
    let empID = document.getElementById("empid").value
    let empDesignation = document.getElementById("empdesignation").value
    let empHours = document.getElementById("emphours").value

    // calculate pay
    if(empDesignation.toLowerCase() == "manager"){
        empSalary = empHours * 50
    } else 
    if (empDesignation.toLowerCase() == "consultant" ){
        empSalary = empHours * 30
    } else
    if(empDesignation.toLowerCase() == "trainee"){
        empSalary = empHours * 20
    } else {
        salary = 0
    }

    // create employee object
    let employee = {
        "name"          : empName,
        "id"            : empID,
        "designation"   : empDesignation,
        "hours"         : empHours,
        "salary"        : empSalary,
       }
 
    // push to array
    employeesArr.push(employee)

    // confirmation message on screen
    text1 = "Save Successful! Added:"
    text2 = "Employee Name = " + empName + ", ID = " + empID + ", Designation = " + empDesignation + ", Hours = " + empHours + ", Salary = " + empSalary
    document.getElementById("text1").innerHTML = text1
    document.getElementById("text2").innerHTML = text2
 
    // clear form fields
    document.getElementById("empname").value = ""
    document.getElementById("empid").value = ""
    document.getElementById("empdesignation").value = ""
    document.getElementById("emphours").value = ""
    
}

function highSalary(){

// First, get the max salary from the array of objects 
var maxSalary = Math.max(...employeesArr.map(e => e.salary));

// Now get the object having this max salary
var obj = employeesArr.find(employee => employee.salary === maxSalary);

// Display message re: highest pay
text1 = "The employee with the highest pay is: " + obj.name + "."
text2 = "This employee is a " + obj.designation + " who worked " + obj.hours + " hours and earned $" + obj.salary + "."
document.getElementById("text1").innerHTML = text1
document.getElementById("text2").innerHTML = text2

}  
var luckyNumber = Math.floor(Math.random() * 10) + 1
var numTries = 1
var numbersGuessed = ""
// alert("Shhh... My lucky number is: " +luckyNumber)


$("#submitbutton").on("click", function(event){
    event.preventDefault();
    var numEntered =  $("#luckynum").val()
    var result = (numEntered - Math.floor(numEntered)) !== 0

    if (numEntered > 10) {
        alert("You entered a number greater than 10. Enter a number between 1 and 10.")
        $("#luckynum").val("") 
    }
    else if (numEntered < 1) {
        alert("You entered a number less than 1. Enter a number between 1 and 10.")
        $("#luckynum").val("") 
    }
    else if (result) {
        alert("You entered a decimal number. Enter a whole number between 1 and 10.")
        $("#luckynum").val("") 
    }
    else if ( numEntered == luckyNumber) {
            $("#message").css("color", "green");
            $('#message').html("Good Job! You guessed my lucky number in " + numTries + " tries.")
            $('#message2').html("MY LUCKY NUMBER IS " + numEntered + "!")
            $("#luckynum").prop( "disabled", true)
            $("#playboard").css("opacity", "0.2")
            $("#playagain").css("visibility", "visible")
    } 
    else {
            numbersGuessed = numbersGuessed.concat(" " + numEntered)
            $("#message").css("color", "red");
            $('#message').html("Sorry! You guessed incorrectly. Try again.")
            $('#message2').html("So far, you have guessed:" + numbersGuessed )
            $("#luckynum").val("") 
            numTries ++
    }
});



$("#playagain").on("click", function(){
    luckyNumber = Math.floor(Math.random() * 10) + 1
    numTries = 1
    numbersGuessed = ""
    $("#luckynum").val("") 
    $("#message").html("") 
    $("#message2").html("") 
    $("#luckynum").prop("disabled", false)
    $("#playboard").css("opacity", "1");
    $("#playagain").css("visibility", "hidden");
    //alert("Shhh... My lucky number is: " + luckyNumber)
});
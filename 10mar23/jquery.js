// function show(){
// $("p").hide()
// }

//same thing written different way
$("#btn1").on("click", () => {   
    $("p").hide()
})

//take everything and change it to jquery. no more documentgetelementbyid and simple functions
// for example

//function addData() was a function that was called on a button click
// change to 
// $("#btn1").on("click", () => {

// })

//look in API documentation for jquery functions


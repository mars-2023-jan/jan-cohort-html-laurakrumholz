var employeesArr = [] // create an array to store employee object when click save
var empName
var empID
var empDesignation
var empHours

    // create employee class
    class Employee {
        #nameValue
        #idValue
        #designationValue
        #hoursValue
        #salaryValue

        constructor (name, id, designation, hours, salary) {
            this.#nameValue = name
            this.#idValue = id
            this.#designationValue = designation
            this.#hoursValue = hours
            this.#salaryValue = salary
        }
        
        get name() {
            return this.#nameValue
        }
        set name(name) {
            if (name === "") {
                alert ("Employee Name cannot be empty")
            } else {
                this.#nameValue = name;
            } 
        }

        get id() {
            return this.#idValue
        }
        set id(id) {
            if (id === "") {
                alert ("Employee ID cannot be empty")
            } else {
                this.#idValue = id;
            } 
        }

        get designation() {
            return this.#designationValue
        }
        set designation(designation) {
            if (designation === "") {
                alert ("Designation cannot be empty")
            } else {
                this.#designationValue = designation;
            } 
        }

        get hours() {
            return this.#hoursValue
        }
        set hours(hours) {
            if (hours === "") {
                alert ("Hours Worked cannot be empty")
            } else {
                this.#hoursValue = hours;
            } 
        }

        get salary() {
            return this.#salaryValue
        }
        set salary(salary) {
            if (salary === "") {
                alert ("Cannot calculate Salary")
            } else {
                this.#salaryValue = salary;
            }
        }
    }


$( "#employeeform" ).on("submit", function( event ) {

    event.preventDefault();
   
   //get values from form
   empName = $("#empname").val()
   empID = $("#empid").val()   
   empDesignation = $("#empdesignation").val() 
   empHours = $("#emphours").val()

   if(empDesignation.toLowerCase() == "manager"){
        empSalary = empHours * 50
    } else 
    if (empDesignation.toLowerCase() == "consultant" ){
        empSalary = empHours * 30
    } else
    if(empDesignation.toLowerCase() == "trainee"){
        empSalary = empHours * 20    
    } else
    {   empSalary = 0
    }

//console.log(empName, empID, empDesignation, empHours, empSalary)
    
   //create new employee using form values
   const emp = new Employee(empName, empID, empDesignation, empHours, empSalary)

    // push to array
    employeesArr.push(emp)

    // confirmation message on screen
    text1 = "Save Successful! Added:"
    text2 = "Employee Name = " + empName + ", ID = " + empID + ", Designation = " + empDesignation + ", Hours = " + empHours + ", Salary = " + empSalary
    $('#text1').html(text1)
    $('#text2').html(text2)
 
    // clear form fields
    $("#empname").val("") 
    $("#empid").val("") 
    $("#empdesignation").val("") 
    $("#emphours").val("") 
    
});

$("#salarybutton").on("click", function(){

//find record with highest pay
const employeeWithHighestSalary = employeesArr.reduce((prev, current) => {return prev.salary > current.salary ? prev : current})
    
console.log(employeeWithHighestSalary)

// Display message re: highest pay
text1 = "The employee with the highest pay is: " + employeeWithHighestSalary.name + "."
text2 = "This employee is a " + employeeWithHighestSalary.designation + " who worked " + employeeWithHighestSalary.hours + " hours and earned $" + employeeWithHighestSalary.salary + "."
$('#text1').html(text1)
$('#text2').html(text2)

}) 

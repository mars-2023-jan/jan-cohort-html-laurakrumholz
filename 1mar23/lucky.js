var luckyNumber = Math.floor(Math.random() * 10) + 1
var numTries = 0
var matchTries = 0
var triesLeft = 3
var numbersGuessed = ""
alert("Shhh... My lucky number is: " +luckyNumber)

function luckyNum() {
    if (numTries < 2) {    
        // alert (triesLeft + " tries left")
        var numEntered = document.getElementById("luckynum").value 
        var result = (numEntered - Math.floor(numEntered)) !== 0
        alert(numEntered)
        if (numEntered > 10) {
            alert("You entered a number greater than 10. Enter a number between 1 and 10.")
            document.getElementById("luckynum").value = ""
        }
        else if (numEntered < 1) {
            alert("You entered a number less than 1. Enter a number between 1 and 10.")
            document.getElementById("luckynum").value = ""
        }
        else if (result) {
            alert("You entered a decimal number. Enter a whole number between 1 and 10.")
            document.getElementById("luckynum").value = ""
        }
        else if (isNaN(numEntered)) {
            alert("You entered a letter or character. Enter a whole number between 1 and 10.")
            document.getElementById("luckynum").value = ""
        }
        else if ( numEntered == luckyNumber) {
                document.getElementById("message").style.color = "green"
                document.getElementById("message").innerHTML = "Good Job! You guessed my lucky number in " + numTries + " tries."
                document.getElementById("message2").innerHTML = "MY LUCKY NUMBER IS " + numEntered + "!"
                document.getElementById("luckynum").disabled = true
                document.getElementById("playboard").style.opacity = "0.2"
                document.getElementById("playagain").style.visibility = "visible"
                matchTries = 0
        } 
        else {
                numTries = (numTries + 1)
                triesLeft = (triesLeft - 1)
                numbersGuessed = numbersGuessed.concat(" " + numEntered)
                document.getElementById("message").style.color = "red"
                document.getElementById("message").innerHTML = "You guessed incorrectly. You have " + triesLeft + " tries left."
                document.getElementById("message2").innerHTML = "(So far, you have guessed these numbers:" + numbersGuessed + ")"
                document.getElementById("luckynum").value = ""

        }
    } else { //numtries >=3 //
        matchTries = (matchTries + 1)
        if (matchTries < 3) {
            document.getElementById("playboard").style.opacity = "0.2"
            document.getElementById("message").style.color = "red"
            document.getElementById("message").innerHTML = "Sorry! My lucky number was " + luckyNumber + "." 
            document.getElementById("message2").innerHTML = "You did not guess it within 3 tries."
            document.getElementById("message3").innerHTML = "You can play again if you would like."
            document.getElementById("playagain").style.visibility = "visible"
            document.getElementById("gameover").style.visibility ="visible"
        }
        else { //matchtries = 3
            document.getElementById("playboard").style.opacity = "0.2"
            document.getElementById("message").style.color = "red"
            document.getElementById("message").innerHTML = "Sorry! My lucky number was " + luckyNumber + "." 
            document.getElementById("message2").innerHTML = "You did not guess it within 3 tries."
            document.getElementById("message3").innerHTML = "That was your final game for now. Goodbye."
            document.getElementById("gameover").style.visibility = "visible"
        }
    }
}

function playAgain() {
    luckyNumber = Math.floor(Math.random() * 10) + 1
    numTries = 0
    triesLeft = 3
    numbersGuessed = ""
    document.getElementById("luckynum").value = ""
    document.getElementById("message").innerHTML = ""
    document.getElementById("message2").innerHTML = ""
    document.getElementById("message3").innerHTML = ""
    document.getElementById("luckynum").disabled = false
    document.getElementById("playboard").style.opacity = "1"
    document.getElementById("playagain").style.visibility = "hidden"
    document.getElementById("gameover").style.visibility = "hidden"
    alert("Shhh... My lucky number is: " + luckyNumber)
}

function gameOver() {
    document.getElementById("playboard").style.visibility = "hidden"
    document.getElementById("message").style.animation = "animate 1.5s linear infinite"
    document.getElementById("message").style.fontSize = "4vh"
    document.getElementById("message").innerHTML = "*** Goodbye! ***"
    document.getElementById("message2").style.visibility = "hidden"
    document.getElementById("message3").style.visibility = "hidden"
    document.getElementById("gameover").style.visibility = "hidden"
    document.getElementById("playagain").style.visibility = "hidden"
}
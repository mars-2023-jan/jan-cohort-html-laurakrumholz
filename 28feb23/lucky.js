var luckyNumber = Math.floor(Math.random() * 10) + 1
var numTries = 1
var numbersGuessed = ""
// alert("Shhh... My lucky number is: " +luckyNumber)

function luckyNum() {
var numEntered = document.getElementById("luckynum").value 
var result = (numEntered - Math.floor(numEntered)) !== 0

if (numEntered > 10) {
    alert("You entered a number greater than 10. Enter a number between 1 and 10.")
    document.getElementById("luckynum").value = ""
}
else if (numEntered < 1) {
    alert("You entered a number less than 1. Enter a number between 1 and 10.")
    document.getElementById("luckynum").value = ""
}
else if (result) {
    alert("You entered a decimal number. Enter a whole number between 1 and 10.")
    document.getElementById("luckynum").value = ""
}
else if ( numEntered == luckyNumber) {
        document.getElementById("message").style.color = "green"
        document.getElementById("message").innerHTML = "Good Job! You guessed my lucky number in " + numTries + " tries.";
        document.getElementById("message2").innerHTML = "MY LUCKY NUMBER IS " + numEntered + "!"
        document.getElementById("luckynum").disabled = true
        document.getElementById("playboard").style.opacity = "0.2"
        document.getElementById("playagain").style.visibility = "visible"
} 
else {
        numbersGuessed = numbersGuessed.concat(" " + numEntered)
        document.getElementById("message").style.color = "red"
        document.getElementById("message").innerHTML = "Sorry! You guessed incorrectly. Try again."
        document.getElementById("message2").innerHTML = "So far, you have guessed:" + numbersGuessed 
        document.getElementById("luckynum").value = ""
        numTries ++
}
}

function playAgain() {
    luckyNumber = Math.floor(Math.random() * 10) + 1
    numTries = 1
    numbersGuessed = ""
    document.getElementById("luckynum").value = ""
    document.getElementById("message").innerHTML = ""
    document.getElementById("message2").innerHTML = ""
    document.getElementById("luckynum").disabled = false
    document.getElementById("playboard").style.opacity = "1"
    document.getElementById("playagain").style.visibility = "hidden"
    //alert("Shhh... My lucky number is: " + luckyNumber)
}
let salaries ={
    "John" : 500,
    "Mary" : 800,
    "Carl" : 600,
}
function maxSalary (salaries){
    let maxSalary = 0
    let maxName = null

    for(const[name, salary] of Object.entries(salaries)){
        if(maxSalary < salary){
            maxSalary = salary
            maxName = name        
        }
        return maxName
    }
}
console.log(Object.keys(salaries))
console.log(Object.values(salaries))

//HW use reduce method to reduce this to a single line using reduce function of arrays
let a = [1,3,5,7]
let b = [2,4,6,8]

let c = [...a,...b]
console.log(c)

//rest above combines two arrays into a new array

let car = {
    make: 'Ford',
    model: 'Mustang',
    year: 1999,
}

let updatedCar= {
    color: 'Red',
    year: 2018,
}

let d = {...car,...updatedCar}

//rest above combines two objects into a new object
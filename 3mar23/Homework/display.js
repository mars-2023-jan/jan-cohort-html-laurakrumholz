function makeTable(){

    var tablearea = document.getElementById('mytable')
    table = document.createElement('table')

    var fNameArr = JSON.parse(localStorage.getItem("fNameData"))
    var lNameArr = JSON.parse(localStorage.getItem("lNameData"))
    var companyArr = JSON.parse(localStorage.getItem("companyData"))
    var emailAddrArr = JSON.parse(localStorage.getItem("emailAddrData"))
    var phoneArr = JSON.parse(localStorage.getItem("phoneData"))
    var phoneTypeArr = JSON.parse(localStorage.getItem("phoneTypeData"))
    var emailMeArr = JSON.parse(localStorage.getItem("emailMeData"))
    var callMeArr = JSON.parse(localStorage.getItem("callMeData"))
    var textMeArr = JSON.parse(localStorage.getItem("textMeData"))
    var reasonArr = JSON.parse(localStorage.getItem("reasonData"))
    var fileNameArr = JSON.parse(localStorage.getItem("fileNameData"))
    var emailOptInArr = JSON.parse(localStorage.getItem("emailOptInData"))
    var textOptInArr = JSON.parse(localStorage.getItem("textOptInData"))

    // BUILD TABLE HEADER
    var tr = document.createElement('tr')
    for (let j=0; j <= 12; j++){
        tr.appendChild( document.createElement('th') )
    }

    tr.cells[0].innerHTML = ("First Name") 
    tr.cells[1].innerHTML = ("Last Name") 
    tr.cells[2].innerHTML = ("Company Name") 
    tr.cells[3].innerHTML = ("Email Address") 
    tr.cells[4].innerHTML = ("Phone Number" ) 
    tr.cells[5].innerHTML = ("Phone Type") 
    tr.cells[6].innerHTML = ("Email?") 
    tr.cells[7].innerHTML = ("Call?") 
    tr.cells[8].innerHTML = ("Text?") 
    tr.cells[9].innerHTML = ("Reason") 
    tr.cells[10].innerHTML = ("Attached File") 
    tr.cells[11].innerHTML = ("Email Opt In") 
    tr.cells[12].innerHTML = ("Text Opt In") 
    table.appendChild(tr);
  
    //LOOP THROUGH ARRAY, CREATING DATA ROWS AS NEEDED
    for (var i=0; i < fNameArr.length; i++) {
        var tr = document.createElement('tr')
    
        for (let j=0; j <= 12; j++){
            tr.appendChild( document.createElement('td') )
        }
    
        tr.cells[0].appendChild( document.createTextNode(fNameArr[i]) )
        tr.cells[1].appendChild( document.createTextNode(lNameArr[i]) )
        tr.cells[2].appendChild( document.createTextNode(companyArr[i]) )
        tr.cells[3].appendChild( document.createTextNode(emailAddrArr[i]) )
        tr.cells[4].appendChild( document.createTextNode(phoneArr[i]) )
        tr.cells[5].appendChild( document.createTextNode(phoneTypeArr[i]) )
        tr.cells[6].appendChild( document.createTextNode(emailMeArr[i]) )
        tr.cells[7].appendChild( document.createTextNode(callMeArr[i]) )
        tr.cells[8].appendChild( document.createTextNode(textMeArr[i]) )
        tr.cells[9].appendChild( document.createTextNode(reasonArr[i]) )
        tr.cells[10].appendChild( document.createTextNode(fileNameArr[i]) )
        tr.cells[11].appendChild( document.createTextNode(emailOptInArr[i]) )
        tr.cells[12].appendChild( document.createTextNode(textOptInArr[i]) )
    
        table.appendChild(tr);
    }
    
    tablearea.appendChild(table);
        
    }
    
   
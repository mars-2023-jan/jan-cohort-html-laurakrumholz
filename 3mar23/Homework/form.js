var fNameArr = []
var lNameArr = []
var companyArr = []
var emailAddrArr = []
var phoneArr = []
var phoneTypeArr = []
var emailMeArr = []
var callMeArr = []
var textMeArr = []
var reasonArr = []
var fileNameArr = []
var emailOptInArr = []
var textOptInArr = []
var textSelected 
var emailSelected 
var emailYesNo 
var textYesNo 
var callYesNo 

function setGlobalValues() {
    textSelected = "Yes"
    emailSelected = "Yes"
    emailYesNo = "Yes"
    textYesNo = "Yes"
    callYesNo = "Yes"
    // alert(textSelected)
    // alert(emailSelected)
    // alert(emailYesNo)
    // alert(textYesNo)
    // alert(callYesNo)
}

function emailChoice() {
    if(document.getElementById("sendemail").checked) {
        //Yes Please radio button is checked
        emailSelected = document.getElementById("sendemail").value
    }else if(document.getElementById("noemail").checked) {
        //No Thank You radio button is checked
        emailSelected = document.getElementById("noemail").value
    }
}

function textChoice() {
    if(document.getElementById("sendtext").checked) {
        //Yes Please radio button is checked
        textSelected = document.getElementById("sendtext").value
    }else if(document.getElementById("notext").checked) {
        //No Thank You radio button is checked
        textSelected = document.getElementById("notext").value
    }
  }

  function emailYN() {
    if (document.getElementById("emailme").checked) {
        //email is okay
        emailYesNo = "Yes"
    }
    else{
        //email not ok
        emailYesNo = "No" 
    }

  }

  function callYN() {
    if (document.getElementById("callme").checked) {
        //call is okay
        callYesNo = "Yes"
    }
    else{
        //call not ok
        callYesNo = "No" 
    }

  }

  function textYN() {
    if (document.getElementById("textme").checked) {
        //text is okay
        textYesNo = "Yes"
    }
    else{
        //text not ok
        textYesNo = "No" 
    }

  }

function addData(){

    let fName = document.getElementById("fname").value
    // alert(fName)
    let lName = document.getElementById("lname").value
    // alert(lName)
    let company = document.getElementById("company").value
    // alert(company)
    let emailAddr = document.getElementById("emailaddr").value
    // alert(emailAddr)
    let phone = document.getElementById("phone").value
    // alert(phone)
    let phoneType = document.getElementById("phonetype").value
    // alert(phoneType)
    let emailMe = emailYesNo
    // alert(emailMe)
    let callMe = callYesNo
    // alert(callMe)
    let textMe = textYesNo
    // alert(textMe)
    let reason = document.getElementById("reason").value
    // alert(reason)
    let fileInput = document.getElementById("filename")
    // alert(fileInput)
    let fileName = fileInput.files[0].name
    // alert(fileName)
    let emailOptIn = emailSelected
    // alert(emailOptIn)
    let textOptIn = textSelected
    // alert(textOptIn)
    
    // alert(fName)
    // alert(lName)
    // alert(company)
    // alert(emailAddr)
    // alert(phone)
    // alert(phoneType)
    // alert(emailMe)
    // alert(callMe)
    // alert(textMe)
    // alert(reason)
    // alert(fileName)
    // alert(emailOptIn)
    // alert(textOptIn)

    fNameArr.push(fName)
    lNameArr.push(lName)
    companyArr.push(company)
    emailAddrArr.push(emailAddr)
    phoneArr.push(phone)
    phoneTypeArr.push(phoneType)
    emailMeArr.push(emailMe)
    callMeArr.push(callMe)
    textMeArr.push(textMe)
    reasonArr.push(reason)
    fileNameArr.push(fileName)
    emailOptInArr.push(emailOptIn)
    textOptInArr.push(textOptIn)

    localStorage.setItem('fNameData', JSON.stringify(fNameArr))
    localStorage.setItem('lNameData', JSON.stringify(lNameArr))
    localStorage.setItem('companyData', JSON.stringify(companyArr))
    localStorage.setItem('emailAddrData', JSON.stringify(emailAddrArr))
    localStorage.setItem('phoneData', JSON.stringify(phoneArr))
    localStorage.setItem('phoneTypeData', JSON.stringify(phoneTypeArr))
    localStorage.setItem('emailMeData', JSON.stringify(emailMeArr))
    localStorage.setItem('callMeData', JSON.stringify(callMeArr))
    localStorage.setItem('textMeData', JSON.stringify(textMeArr))
    localStorage.setItem('reasonData', JSON.stringify(reasonArr))
    localStorage.setItem('fileNameData', JSON.stringify(fileNameArr))
    localStorage.setItem('emailOptInData', JSON.stringify(emailOptInArr))
    localStorage.setItem('textOptInData', JSON.stringify(textOptInArr))
   
}

/* code to run productInsertUpdateDelete */
use mars_jan;
set @actionToDo = 'delete';  /* valid options: insert, update, delete */
set @prod_id = 'EL900';
set @prod_name = 'Testy';
set @prod_desc = 'Testy';
set @price = 2000;

CALL productInsertUpdateDelete(
@actionToDo,
@prod_id,
@prod_name,
@prod_desc,
@price
);
select * from product;

/* code to run orderInsertUpdateDelete */
use mars_jan;
set @actionToDo = 'delete';  /* valid options: insert, update, delete */
set @ord_id = 'ORD123';
set @ord_date = '2020-01-01';
set @ord_type = 'Small';
set @prd_id = 'EL500';

CALL orderInsertUpdateDelete(
@actionToDo,
@ord_id,
@ord_date,
@ord_type,
@prd_id
);
select * from order1;
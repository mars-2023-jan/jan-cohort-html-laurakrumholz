CREATE DEFINER=`root`@`localhost` PROCEDURE `orderInsertUpdateDelete`(
IN actionToDo varchar(20),
IN ord_id varchar(10),
IN ord_date date,
IN ord_type varchar(10),
IN prd_id varchar(6))
BEGIN

DECLARE toDo varchar(20);
DECLARE ordId varchar(10);
DECLARE ordDate date;
DECLARE ordType varchar(10);
DECLARE prdId varchar(6);
DECLARE ordToFind varchar(10);

SET toDo = actionToDo;
SET ordId = ord_id;
SET ordDate = ord_date;
SET ordType = ord_type;
SET prdId = prd_id;
SET ordToFind = NULL;

  CASE toDo
  
	WHEN 'insert' THEN
		IF (SELECT EXISTS(SELECT * from order1 WHERE order1.ord_id = ordId) = 0) THEN	 /* ord_id doesn't exist already - add it */
			IF (SELECT EXISTS(SELECT * from product WHERE product.prod_id = prdId) = 1) THEN /* foreign key prd_id exists - continue */
				INSERT INTO order1
					(ord_id,
					ord_date,
					ord_type,
					prd_id)
				VALUES
					(ordId,
					ordDate,
					ordType,
					prdId);
			ELSE
				SELECT CONCAT('The prd_id entered is not a valid prd_id. Choose a value from product table : ', prdId) as 'error';
			END IF;
		ELSE
			SELECT CONCAT('The ord_id entered already exists. Choose a unique value: ', ordId) as 'error';
		END IF;

	WHEN 'update' THEN
		IF (SELECT EXISTS(SELECT * from order1 WHERE order1.ord_id = ordId) = 1) THEN	/* ord_id exists - update it */
			IF (SELECT EXISTS(SELECT * from product WHERE product.prod_id = prdId) = 1) THEN /* foreign key prd_id exists - continue */
				UPDATE order1
				SET
					ord_date = ordDate,
					ord_type = ordType,
					prd_id = prdId
				WHERE order1.ord_id = ordId;
            ELSE
				SELECT CONCAT('The prd_id entered is not a valid prd_id. Choose a value from product table ', prdId) as 'error';
			END IF;    
		ELSE
			SELECT CONCAT('The ord_id entered does not exist in the table. Cannot update: ', ordId) as 'error';
		END IF;
    
    WHEN 'delete' THEN
		IF (SELECT EXISTS(SELECT * from order1 WHERE order1.ord_id = ordId) = 1) THEN /* ord_id exists - delete it */
			DELETE FROM order1
			WHERE order1.ord_id = ordId; 
		ELSE
			SELECT CONCAT('The ord_id entered does not exist in the table. Cannot delete: ', ordId) as 'error';
        END IF;
	END CASE;

END
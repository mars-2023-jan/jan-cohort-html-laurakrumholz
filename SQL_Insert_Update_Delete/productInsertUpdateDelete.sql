CREATE DEFINER=`root`@`localhost` PROCEDURE `productInsertUpdateDelete`(
IN actionToDo varchar(20),
IN prod_id varchar(6),
IN prod_name varchar(10),
IN prod_desc varchar(20),
IN price decimal(8,2))
BEGIN

DECLARE toDo varchar(20);
DECLARE prodId varchar(6);
DECLARE prodName varchar(10);
DECLARE prodDesc varchar(20);
DECLARE prodPrice decimal(8,2);

SET toDo = actionToDo;
SET prodId = prod_id;
SET prodName = prod_name;
SET prodDesc = prod_desc;
SET prodPrice = price;

  CASE toDo
  
	WHEN 'insert' THEN
		IF (SELECT EXISTS(SELECT * from product WHERE product.prod_id = prodId) = 0) THEN	 /* prod_id doesn't exist already - add it */
			INSERT INTO product
				(prod_id,
				prod_name,
				prod_desc,
				price)
			VALUES
				(prodId,
				prodName,
				prodDesc,
				prodPrice);
		ELSE
			SELECT CONCAT('The prod_id entered already exists. Choose a unique value: ', prodId) as 'error';
		END IF;

	WHEN 'update' THEN
		IF (SELECT EXISTS(SELECT * from product WHERE product.prod_id = prodId) = 1) THEN	/* prod_id exists - update it */
			UPDATE product
			SET
				prod_name = prodName,
				prod_desc = prodDesc,
				price = ProdPrice
			WHERE product.prod_id = prodId;
		ELSE
			SELECT CONCAT('The prod_id entered does not exist in the table. Cannot update: ', prodId) as 'error';
		END IF;
    
    WHEN 'delete' THEN
		IF (SELECT EXISTS(SELECT * from product WHERE product.prod_id = prodId) = 1) THEN /* prod_id exists - delete it */
			DELETE FROM product
			WHERE product.prod_id = prodId; 
		ELSE
			SELECT CONCAT('The prod_id entered does not exist in the table. Cannot delete: ', prodId) as 'error';
        END IF;
	END CASE;

END